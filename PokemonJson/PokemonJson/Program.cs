﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace PokemonJson
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("','");
            string fileInAtkPath = "../../../pokemonAttack.txt";
            string fileOutAtkPath = "../../../pokemonAttack.json";
            string fileInAbilityPath = "../../../pokemonAbility.txt";
            string fileOutAbilityPath = "../../../pokemonAbility.json";
            string fileInTMPath = "../../../pokemonTM.txt";
            string fileOutTMPath = "../../../pokemonTM.json";
            string fileInHMPath = "../../../pokemonHM.txt";
            string fileOutHMPath = "../../../pokemonHM.json";
            string fileInPokedexPath = "../../../pokemonPokedex.txt";
            string fileOutPokedexPath = "../../../pokemonPokedex.json";
            string fileInLevelLearnsetPath = "../../../pokemonLearnset.txt";
            string fileInHMLearnsetPath = "../../../pokemonHMlearnset.txt";
            string fileInTMLearnsetPath = "../../../pokemonTMlearnset.txt";
            generateAttackJSON(fileInAtkPath, fileOutAtkPath);
            generateAbilityJSON(fileInAbilityPath, fileOutAbilityPath);
            generateTMJSON(fileInTMPath, fileOutTMPath);
            generateTMJSON(fileInHMPath, fileOutHMPath);
            generatePokedexJSON(fileInPokedexPath, fileInHMLearnsetPath, fileInTMLearnsetPath, fileInLevelLearnsetPath,  fileOutPokedexPath);
        }

        public static void generateAttackJSON(string inputPath, string outputPath)
        {
            string nameEN;
            string nameFR;
            string descEN;
            string descFR;
            string type;
            string category;
            string power;
            string accuracy;
            string pp;
            int id = 1;

            string str = "[";

            string[] lines = File.ReadAllLines(inputPath);
            foreach (string line in lines)
            {
                string[] values = line.Split("',");
                nameEN = values[0].Remove(0, 1);
                nameFR = values[1].Remove(0, 1);
                descEN = values[2].Remove(0, 1);
                descFR = values[3].Remove(0, 1);
                string[] rests = values[4].Split(',');
                type = rests[0];
                category = rests[1];
                power = rests[2];
                accuracy = rests[3];
                pp = rests[4];

                str += "\n\t{\n\t\t\"id\":" + id + ",\n\t\t\"name\": {\n\t\t\t\"english\": \"" + nameEN + "\",\n\t\t\t\"french\": \"" + nameFR + "\"\n\t\t},\n\t\t\"description\": {\n\t\t\t\"english\": \"" + descEN + "\",\n\t\t\t\"french\": \"" + descFR + "\"\n\t\t},\n\t\t\"type\": " + type + ",\n\t\t\"category\": " + category + ",\n\t\t\"power\": " + power + ",\n\t\t\"accuracy\": " + accuracy + ",\n\t\t\"pp\": " + pp + "\n\t},";
                id++;
            }
            str = str.Remove(str.Length - 1);// remove last ,
            str += "\n]";
            Console.WriteLine(str);
            File.WriteAllText(outputPath, str);
        }

        public static void generateAbilityJSON(string inputPath, string outputPath)
        {
            string nameEN;
            string nameFR;
            string descEN;
            string descFR;
            int id = 1;

            string str = "[";

            string[] lines = File.ReadAllLines(inputPath);
            foreach (string line in lines)
            {
                string[] values = line.Split("',");
                nameEN = values[0].Remove(0, 1);
                nameFR = values[1].Remove(0, 1);
                descEN = values[2].Remove(0, 1);
                descFR = values[3].Remove(0, 1);
            
                str += "\n\t{\n\t\t\"id\":" + id + ",\n\t\t\"name\": {\n\t\t\t\"english\": \"" + nameEN + "\",\n\t\t\t\"french\": \"" + nameFR + "\"\n\t\t},\n\t\t\"description\": {\n\t\t\t\"english\": \"" + descEN + "\",\n\t\t\t\"french\": \"" + descFR + "\"\n\t\t}\n\t},";
                id++;
            }

            str = str.Remove(str.Length - 1);// remove last ,
            str += "\n]";
            Console.WriteLine(str);
            File.WriteAllText(outputPath, str);
        }

        public static void generateTMJSON(string inputPath, string outputPath) //Also work for HM (same raw format)
        {
            string nameEN;
            string nameFR;
            int attack;
            int id = 1;

            string str = "[";

            string[] lines = File.ReadAllLines(inputPath);
            foreach (string line in lines)
            {
                string[] values = line.Split("',");
                nameEN = values[0].Remove(0, 1);
                nameFR = values[1].Remove(0, 1);
                string[] rest = values[2].Split(" -- ");
                Console.WriteLine(rest[0]);
                attack = int.Parse(rest[0]);

                str += "\n\t{\n\t\t\"id\":" + id + ",\n\t\t\"name\": {\n\t\t\t\"english\": \"" + nameEN + "\",\n\t\t\t\"french\": \"" + nameFR + "\"\n\t\t},\n\t\t\"attack\": " + attack + "\n\t},";
                id++;
            }

            str = str.Remove(str.Length - 1);// remove last ,
            str += "\n]";
            Console.WriteLine(str);
            File.WriteAllText(outputPath, str);
        }

        public static void generatePokedexJSON(string inputPath, string outputPath) //attack list blank -- multiple evolution not available (cf input format)
        {
            string nameEN;
            string nameFR;
            int type1;
            int type2;
            int evolutionId;
            //bool evolutionSpecific; //TODO modification.
            int evolutionLvl;
            double size;
            double weight;
            int ability1;
            int ability2;
            int ability3;
            int catchRate;
            int baseStatHP;
            int baseStatAtk;
            int baseStatDef;
            int baseStatSpeAtk;
            int baseStatSpeDef;
            int baseStatSpeed;

            int id = 1;

            string str = "[";

            string[] lines = File.ReadAllLines(inputPath);
            foreach (string line in lines)
            {
                string[] values = line.Split(",");
                nameEN = values[0].Remove(0, 1).TrimEnd('\''); //remove both '
                nameFR = values[1].Remove(0, 1).TrimEnd('\''); //remove both '
                str += "\n\t{\n\t\t\"id\":" + id + ",\n\t\t\"name\": {\n\t\t\t\"english\": \"" + nameEN + "\",\n\t\t\t\"french\": \"" + nameFR + "\"\n\t\t},";
                type1 = int.Parse(values[2]);
                str += "\n\t\t\"type\": [\n\t\t\t{\n\t\t\t\"id\": " + type1 + "\n\t\t\t}"; 
                // CAN BE NULL
                if (values[3] != "null")
                {
                    type2 = int.Parse(values[3]);
                    str += ",\n\t\t\t{\n\t\t\t\"id\": " + type2 + "\n\t\t\t}";
                }
                else
                {
                    type2 = 0;
                }
                str += "\n\t\t],";
                str += "\n\t\t\"evolution\": [";
                // CAN BE NULL
                if (values[4] != "null")
                {
                    evolutionId = int.Parse(values[4]);
                    str += "\n\t\t\t{\n\t\t\t\t\"id\": " + evolutionId + ",";
                }
                else
                {
                    evolutionId = 0;
                }
                // CAN BE NULL
                if (values[5] == "'false'")
                {
                    //evolutionSpecific = false;
                    if(evolutionId != 0) //write only if there is an evolution.
                    {
                        str += "\n\t\t\t\t\"special_evolution\": false,";
                    }
                }
                else
                {
                    //evolutionSpecific = true;
                    if (evolutionId != 0) //write only if there is an evolution.
                    {
                        str += "\n\t\t\t\t\"special_evolution\": true,";
                    }
                }
                // CAN BE NULL
                if (values[6] != "null")
                {
                    evolutionLvl = int.Parse(values[6]);

                    if (evolutionId != 0) //write only if there is an evolution.
                    {
                        str += "\n\t\t\t\t\"level\": " + evolutionLvl+ "\n\t\t\t}\n\t\t";
                    }
                }
                else
                {
                    evolutionLvl= 0;
                    if (evolutionId != 0) //write only if there is an evolution.
                    {
                        str += "\n\t\t\t\t\"level\": null\n\t\t\t}\n\t\t";
                    }
                }

                str += "],";
                // Parse with '.' as sperator.
                size = double.Parse(values[7], CultureInfo.InvariantCulture);
                weight = double.Parse(values[8], CultureInfo.InvariantCulture);
                
                ability1 = int.Parse(values[9]);
                str += "\n\t\t\"abilities\": [\n\t\t\t{\n\t\t\t\t\"id\":" + ability1 + "\n\t\t\t}";
                // CAN BE NULL
                if (values[10] != "null")
                {
                    ability2 = int.Parse(values[10]);
                    str += ",\n\t\t\t{\n\t\t\t\t\"id\":" + ability2 + "\n\t\t\t}";
                }
                else
                {
                    ability2 = 0;
                }
                // CAN BE NULL
                if (values[11] != "null")
                {
                    ability3 = int.Parse(values[11]);
                    str += ",\n\t\t\t{\n\t\t\t\t\"id\":" + ability3 + "\n\t\t\t}";
                }
                else
                {
                    ability3 = 0;
                }
                str += "\n\t\t],";
                catchRate = int.Parse(values[12]);
                str += "\n\t\t\"catchRate\": " + catchRate;
                baseStatHP = int.Parse(values[13]);
                baseStatAtk = int.Parse(values[14]);
                baseStatDef = int.Parse(values[15]);
                baseStatSpeAtk = int.Parse(values[16]);
                baseStatSpeDef = int.Parse(values[17]);
                baseStatSpeed = int.Parse(values[18]);

                
                str += ",\n\t\t\"base_stats\": {\n\t\t\t\"hp\":" + baseStatHP + ",\n\t\t\t\"atk\":" + baseStatAtk + ",\n\t\t\t\"def\":" + baseStatDef + ",\n\t\t\t\"speAtk\":" + baseStatSpeAtk + ",\n\t\t\t\"speDef\":" + baseStatSpeDef + ",\n\t\t\t\"speed\":" + baseStatSpeed + "\n\t\t},";
                // allocating space for future modification.
                str += "\n\t\t\"moves\": {\n\t\t\t\"level_up\": [\n\n\t\t\t],\n\t\t\t\"hm\": [\n\n\t\t\t],\n\t\t\t\"tm\": [\n\n\t\t\t]\n\t\t}";

                id++;
                str += "\n\t},";
            }

            str = str.Remove(str.Length - 1);// remove last coma
            str += "\n]";
            //Console.WriteLine(str);
            File.WriteAllText(outputPath, str);
        }

        public static void generatePokedexJSON(string inputPath1, string inputPath2, string inputPath3, string inputPath4, string outputPath) //attack list blank -- multiple evolution not available (cf input format)
        {
            string nameEN;
            string nameFR;
            int type1;
            int type2;
            int evolutionId;
            //bool evolutionSpecific; //TODO modification.
            int evolutionLvl;
            double size;
            double weight;
            int ability1;
            int ability2;
            int ability3;
            int catchRate;
            int baseStatHP;
            int baseStatAtk;
            int baseStatDef;
            int baseStatSpeAtk;
            int baseStatSpeDef;
            int baseStatSpeed;

            // Learnable attack HM/TM & by level.
            var listHM = new List<Tuple<int, int>>();
            var listTM = new List<Tuple<int, int>>();
            var listAttackLevel = new List<Tuple<int, int, int>>();

            //get list from txt files
            //hm file
            string[] linesHM = File.ReadAllLines(inputPath2);
            int valHM0 = 0;
            foreach (string lineHM in linesHM)
            {
                string[] values = lineHM.Split("\t"); //values[0] = HM number - values[1] = pokemon id
                if (values[0] != "--")
                {
                    valHM0 = int.Parse(values[0].Substring(2)); //format CS01 -> 01
                }
                listHM.Add(Tuple.Create(valHM0, int.Parse(values[1])));
            }
            // tm file
            string[] linesTM = File.ReadAllLines(inputPath3);
            int valTM0 = 0;
            foreach (string lineTM in linesTM)
            {
                string[] values = lineTM.Split("\t"); //values[0] = TM number - values[1] = pokemon id
                if (values[0] != "--")
                {
                    valTM0 = int.Parse(values[0].Substring(2)); //format CS01 -> 01
                }
                listTM.Add(Tuple.Create(valTM0, int.Parse(values[1])));
                //Console.WriteLine(valTM0 + " " + int.Parse(values[1]));
            }
            // level file
            string[] linesLVL = File.ReadAllLines(inputPath4);
            int pokeId = 1;
            foreach(string lineLVL in linesLVL)
            {
                if(lineLVL == "--")
                {
                    pokeId++;
                }
                else
                {
                    string[] values = lineLVL.Split("\t");
                    listAttackLevel.Add(Tuple.Create(pokeId, int.Parse(values[0]), int.Parse(values[1]))); // pokeId - values[0] = attackId - values[1] = level
                    //Console.WriteLine(pokeId +" "+ values[0] +" "+ values[1]);
                }
            }

            //writing final file
            int id = 1;

            string str = "[";

            string[] lines = File.ReadAllLines(inputPath1);
            foreach (string line in lines)
            {
                string[] values = line.Split(",");
                nameEN = values[0].Remove(0, 1).TrimEnd('\''); //remove both '
                nameFR = values[1].Remove(0, 1).TrimEnd('\''); //remove both '
                str += "\n\t{\n\t\t\"id\":" + id + ",\n\t\t\"name\": {\n\t\t\t\"english\": \"" + nameEN + "\",\n\t\t\t\"french\": \"" + nameFR + "\"\n\t\t},";
                type1 = int.Parse(values[2]);
                str += "\n\t\t\"type\": [\n\t\t\t{\n\t\t\t\"id\": " + type1 + "\n\t\t\t}";
                // CAN BE NULL
                if (values[3] != "null")
                {
                    type2 = int.Parse(values[3]);
                    str += ",\n\t\t\t{\n\t\t\t\"id\": " + type2 + "\n\t\t\t}";
                }
                else
                {
                    type2 = 0;
                }
                str += "\n\t\t],";
                str += "\n\t\t\"evolution\": [";
                // CAN BE NULL
                if (values[4] != "null")
                {
                    evolutionId = int.Parse(values[4]);
                    str += "\n\t\t\t{\n\t\t\t\t\"id\": " + evolutionId + ",";
                }
                else
                {
                    evolutionId = 0;
                }
                // CAN BE NULL
                if (values[5] == "'false'")
                {
                    //evolutionSpecific = false;
                    if (evolutionId != 0) //write only if there is an evolution.
                    {
                        str += "\n\t\t\t\t\"special_evolution\": false,";
                    }
                }
                else
                {
                    //evolutionSpecific = true;
                    if (evolutionId != 0) //write only if there is an evolution.
                    {
                        str += "\n\t\t\t\t\"special_evolution\": true,";
                    }
                }
                // CAN BE NULL
                if (values[6] != "null")
                {
                    evolutionLvl = int.Parse(values[6]);

                    if (evolutionId != 0) //write only if there is an evolution.
                    {
                        str += "\n\t\t\t\t\"level\": " + evolutionLvl + "\n\t\t\t}\n\t\t";
                    }
                }
                else
                {
                    evolutionLvl = 0;
                    if (evolutionId != 0) //write only if there is an evolution.
                    {
                        str += "\n\t\t\t\t\"level\": null\n\t\t\t}\n\t\t";
                    }
                }

                str += "],";
                // Parse with '.' as sperator.
                size = double.Parse(values[7], CultureInfo.InvariantCulture);
                weight = double.Parse(values[8], CultureInfo.InvariantCulture);

                ability1 = int.Parse(values[9]);
                str += "\n\t\t\"abilities\": [\n\t\t\t{\n\t\t\t\t\"id\":" + ability1 + "\n\t\t\t}";
                // CAN BE NULL
                if (values[10] != "null")
                {
                    ability2 = int.Parse(values[10]);
                    str += ",\n\t\t\t{\n\t\t\t\t\"id\":" + ability2 + "\n\t\t\t}";
                }
                else
                {
                    ability2 = 0;
                }
                // CAN BE NULL
                if (values[11] != "null")
                {
                    ability3 = int.Parse(values[11]);
                    str += ",\n\t\t\t{\n\t\t\t\t\"id\":" + ability3 + "\n\t\t\t}";
                }
                else
                {
                    ability3 = 0;
                }
                str += "\n\t\t],";
                catchRate = int.Parse(values[12]);
                str += "\n\t\t\"catchRate\": " + catchRate;
                baseStatHP = int.Parse(values[13]);
                baseStatAtk = int.Parse(values[14]);
                baseStatDef = int.Parse(values[15]);
                baseStatSpeAtk = int.Parse(values[16]);
                baseStatSpeDef = int.Parse(values[17]);
                baseStatSpeed = int.Parse(values[18]);


                str += ",\n\t\t\"base_stats\": {\n\t\t\t\"hp\":" + baseStatHP + ",\n\t\t\t\"atk\":" + baseStatAtk + ",\n\t\t\t\"def\":" + baseStatDef + ",\n\t\t\t\"speAtk\":" + baseStatSpeAtk + ",\n\t\t\t\"speDef\":" + baseStatSpeDef + ",\n\t\t\t\"speed\":" + baseStatSpeed + "\n\t\t},";
                // allocating space for future modification.
                //level up
                str += "\n\t\t\"moves\": {\n\t\t\t\"level_up\": [";
                foreach (var item in listAttackLevel)
                {
                    if( item.Item1 == id)
                    {
                        str += "\n\t\t\t\t{\n\t\t\t\t\t\"id\": " + item.Item2 + ",\n\t\t\t\t\t\"level\": " + item.Item3 + "\n\t\t\t\t},";
                    }
                }
                str = str.Remove(str.Length - 1);// remove last coma

                //hm
                str += "\n\t\t\t],\n\t\t\t\"hm\": [";
                foreach(var item in listHM)
                {
                    if(item.Item2 == id)
                    {
                        str += "\n\t\t\t\t{\n\t\t\t\t\t\"id\": " + item.Item1 + "\n\t\t\t\t},";
                    }
                }
                if(str.Substring(str.Length -1) == ",")
                {
                    str = str.Remove(str.Length - 1);// remove last coma
                }
                

                //tm
                str += "\n\t\t\t],\n\t\t\t\"tm\": [";
                foreach (var item in listTM)
                {
                    if (item.Item2 == id)
                    {
                        str += "\n\t\t\t\t{\n\t\t\t\t\t\"id\": " + item.Item1 + "\n\t\t\t\t},";
                    }
                }
                if (str.Substring(str.Length - 1) == ",")
                {
                    str = str.Remove(str.Length - 1);// remove last coma
                }

                str += "\n\t\t\t]\n\t\t}";
                id++;
                str += "\n\t},";
            }

            str = str.Remove(str.Length - 1);// remove last coma
            str += "\n]";
            Console.WriteLine(str);
            File.WriteAllText(outputPath, str);
            
        }

    }
}
